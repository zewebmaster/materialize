### Description
Ce thème illustre les composantes du thème pour l'intégration de votre site :

- la déclaration des régions et des points de rupture ;
- la configuration de l'installation et l'instanciation des blocs natifs dans les régions de votre thème ;
- la déclaration des librairies css et js ;
- la déclaration des templates ;
- la déclaration des fonctions de preprocess et de suggestion de thèmes ;

Il fonctionne avec le framework html Materializecss : https://materializecss.com/

Il reprend l'architecture du thème Bartik, disponible nativement avec Drupal.

Pour pouvoir utiliser facilement ce thème (ou un autre), vous devez ABSOLUMENT configurer votre système en mode développement afin de ne pas être gêner par le cache de Drupal.
Tutoriel pour passer en mode développement : https://www.drupal.org/node/2598914

### Installation

Placez ce thème dans le dossier /themes/custom.
Videz le cache.
Activez le thème en back office


### Auteur/Licence

David BOCQUET : https://zewabmaster.fr
mail : david@zewebmaster.fr

Licence CC0
https://creativecommons.org/
