jQuery(document).ready(
  function()
  {
    console.log('script.js');

    // ACTIVATION UTILITAIRE JAVASCRIPT
    activate_jquery_functionnality();
    // cf. API materializecss : https://materializecss.com
    //
    // gestion du menu
    // initialisation de la variable au chargement de la page
    var last_scroll_position = 0;
    jQuery(window).scroll(

      function(event) {
        // récupération de la position courante
        var current_position = jQuery(this).scrollTop();

        // menu connexion
        if( current_position > 0)
        {
          jQuery(  "nav#block-materialize-account-menu" ).css( "display", "none" );
        }
        else
        {
          jQuery( "nav#block-materialize-account-menu" ).css( "display", "block" );
        }
        // menu principal
        if( current_position > 70)
        {
          jQuery(  "nav#main-menu" ).css( "display", "none" );
        }
        // scroll up
        // affichage du menu
        if (current_position < last_scroll_position)
        {
          jQuery(  "nav#main-menu" ).css( "display", "block" );
        }
        else
        {
            // scroll down
        }
        // reinitialisation variable
        last_scroll_position = current_position;
      }
    );

  });



function activate_jquery_functionnality()
{

  // mobile
  jQuery('.sidenav').sidenav();
  jQuery('.modal').modal({
    endingTop : '45%'
  });

  // dropdown
  jQuery('.dropdown-trigger').dropdown(
    {
      constrainWidth: false,    // if true, constrainWidth to the size of the dropdown activator.
      coverTrigger: false       // If false, the dropdown will show below the trigger.
    }
  );

}
