<?php

//
// déclaration des classes nécéssaires au traitement
//




/**
 * Implements hook_preprocess_block().
 * @param  array             $variables
 */
function materialize_preprocess_block(&$variables)
{
  // récupération du type de bloc
  $block_content = $variables['elements']['content'];
  $type          = get_block_type($block_content);

  // dump($type);
}
// fin function materialize_preprocess_block()





/**
* Implements hook_theme_suggestions_HOOK_alter().
*/
function materialize_theme_suggestions_block_alter(array &$suggestions, array $variables) {

  // récupération du type de bloc
  $block_content = $variables['elements']['content'];
  $type          = get_block_type($block_content);

  $suggestions[] = 'block__' . $type;
}
// fin function materialize_theme_suggestions_block_alter




/**
 * récupére le type du block
 * @param  array   $block_content     le contenu du bloc
 * @return string                     le type du bloc
 */
function get_block_type($block_content)
{
  $type = '';

  // récupération du contenu du bloc
  if(isset($block_content['#block_content']))
  {
    // récupération du type de bloc
    $content       = $block_content['#block_content']->toArray();
    $block_type    = $content['type'];
    $block_type    = reset($block_type);
    $type          = $block_type['target_id'];
  }

  return $type;
}
