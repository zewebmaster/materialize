<?php

//
// déclaration des classes nécéssaires au traitement
//


// pour gérer les liens
use Drupal\Core\Url;


function materialize_preprocess_menu_local_task(&$variables)
{
  // dump($variables);
  $url   = $variables['link']['#url'];
  $path  = $url->getInternalPath();
  $title = $variables['link']['#title'];
  $link  = array(
              'href'  => '/'. $path,
              'title' => $title,
            );
            // dump($path);
  $variables['custom_link'] = $link;
}




/**
 * Implements hook_theme_suggestions_HOOK_alter() for menu templates.
 * @param  array             $suggestions
 * @param  array             $variables
 */
function materialize_theme_suggestions_menu_alter(array &$suggestions, array $variables) {

  if (isset($variables['attributes']['block']))
  {
    $hook = str_replace(array('block-', '-'), array('', '_'), $variables['attributes']['block']);
    $suggestions[] = $variables['theme_hook_original'] . '__' . $hook;
  }
}
