<?php

//
// déclaration des classes nécéssaires au traitement
//




/**
 * Implements hook_preprocess_page
 * @param  array             $variables
 */
function materialize_preprocess_page(&$variables)
{
  // $node = $variables['node'];

  // kint($variables['node']);
  // kint($variables);

  // Ne concerne pas la page d'accueil
  if($variables['is_front'])
  {
    $variables['#attached']['library'][] = 'materialize/front-page';
  }
}
// fin materialize_preprocess_page()







/**
* Implements hook_theme_suggestions_HOOK_alter().
*/
function materialize_theme_suggestions_page_alter(array &$suggestions, array $variables)
{
  // chargement du type de node
  // template surchageagle par simple implémentation de page--$content_type.tpl.php
  if ($node = \Drupal::routeMatch()->getParameter('node'))
  {
    $content_type = $node->bundle();
    $suggestions[] = 'page__'.$content_type;
  }

  //
  // pour les pages erreur
  //
  $status = \Drupal::requestStack()->getCurrentRequest()->attributes->get('exception');
  // Si un status existe et que ce status est le 404 declare custom twig template
  if ($status && $status->getStatusCode() == 404)
  {
    $suggestions[] = 'page__404';
  }
  // Si un status existe et que ce status est le 403 declare custom twig template
  if ($status && $status->getStatusCode() == 403)
  {
    $suggestions[] = 'page__403';
  }
}
// fin function materialize_theme_suggestions_page_alter()
